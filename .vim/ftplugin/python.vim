nmap ,a :w<cr>:!clear; python %<cr>
nmap ,m :w<cr>:!clear; mypy %<cr>
nnoremap ,d "=strftime("%c")<CR>P

" vim test
nmap <silent> <leader>T :TestNearest<CR>
nmap <silent> <leader>t :TestFile -s<CR>
" nmap <silent> <leader>A :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>g :TestVisit<CR>
let test#python#runner = 'pytest'
