alias ls='ls -h -g --time-style=+"%d-%m-%Y %H:%M:%S" --color=auto --group-directories-first'
set -U fish_user_paths /usr/local/bin/go
fish_vi_key_bindings
