## Install yadm
`sudo apt install yadm`

## Clone this repo into yadm
`yadm clone https://gitlab.com/rosscosack/dotfiles.git`

## Install vundle (package manager for vim)
`git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim`

## Create directories for vim
`mkdir ~/.vim/backup ~/.vim/swap`

## Install vim plugins
Within vim
`:PluginInstall`
